// SECTION - JS Synchronous  vs Asynchronous
// Javascript is by default synchronous (only one statement wi;; be executed at a time)
// When a statement has an error, JS will not proceed with the next statement


// console.log("Hell World");
// conosle.log("Hello Again");
// console.log("Goodbye");

// for (let i = 1; i <= 1500; i++) {
//   console.log(i);
// };

// console.log("It's me again");

// Section : ASYNCHRONOUS

// Fetch API - allows us to asynchronously request for a resource (data);
// A "Promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

// asynchronous
fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => console.log(response.status));

fetch("https://jsonplaceholder.typicode.com/posts")
.then(response => response.json())
.then(json => console.log(json));

console.log("Hello");
console.log("Hello World");
console.log("Hello Again");

// Section : ASYNC-AWAIT
// "async" adn "await" keywords are other approach that can be used to perform asynchronous javascript 
// used in functions to indicate which portions of the code should be waited for 


// asynchronous - fetch

async function fetchData() {
  let result = await fetch("https://jsonplaceholder.typicode.com/posts");
  console.log(result);
  console.log(typeof result);
  console.log(result.body);

  let json = await result.json();
  console.log(json);
};

fetchData ();
console.log("Hello Async");



fetch("https://jsonplaceholder.typicode.com/posts", {
  method: "POST",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "New Post",
    body: "Hey World",
    userID: 1
  })
})
.then(response => response.json())
.then(json => console.log(json));


// **mini activity
// ^ SECTION - Updating post (PUT)
// updates a specific post following the RES API (update, /posts/:id, PUT) 

// ^ SECTION : (PUT)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "PUT",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    id: 1,
    title: "Updated Post",
    body: "Hello again",
    userId: 1
  })
})
.then(response => response.json())
.then(json => console.log(json));

// ^ SECTION : (PATCH)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json"
  },
  body: JSON.stringify({
    title: "Corrected Post",
  })
})
.then(response => response.json())
.then(json => console.log(json));

// ^ SECTION : (DELETE)
fetch("https://jsonplaceholder.typicode.com/posts/1", {
  method: "DELETE",
})

// ^ SECTION : (FILTER)
fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
then.(response => response.json())
.then(Json => console.log(json));

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
then.(response => response.json())
.then(Json => console.log(json));